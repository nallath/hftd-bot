#!/usr/bin/env python

from hftd.DefaultImports import *

import dotenv
import hftd

logging.basicConfig(level = logging.INFO)
logging.captureWarnings(True)

dotenv.load_dotenv()

def configure(binder: injector.Binder):
    binder.install(hftd.Module())
    binder.bind(hftd.Token, to = os.getenv("DISCORD_TOKEN"))
    binder.bind(hftd.DataPath, to = Path(os.getenv("BOT_DATA_PATH")))

injector = injector.Injector(configure)
bot = injector.get(hftd.Bot)
bot.add_cogs(
    injector.get(hftd.cogs.Bot),
    injector.get(hftd.cogs.Money),
    injector.get(hftd.cogs.Fame),
)

servers_cog = hftd.servers.ServersCog(bot)
bot.add_cogs(servers_cog)

bot.run()

if bot.restarting:
    exit(234)

