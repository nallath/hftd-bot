from pathlib import Path

import pytest
import hftd

@pytest.mark.asyncio
async def test_load():
    types = hftd.servers.ThreatTypes(Path(__file__).with_name("threats.yml"))
    await types.load()

    type1 = types.get("test_type_1")
    assert type1
    assert type1.name == "Test Type 1"
    assert type1.initial == "Initial message for Test Type 1"
    assert type1.health == -1
    assert type1.damage_resist == 0
    assert type1.steps == []

    type2 = types.get("test_type_2")
    assert type2.health == 5

    type3 = types.get("test_type_3")
    assert type3.damage_resist == 2

    type4 = types.get("test_type_4")
    assert len(type4.steps) == 3

    step0 = type4.steps[0]
    assert step0.offset == 2
    assert step0.action == "test"
    assert step0.value is None
    assert step0.target is None

    step1 = type4.steps[1]
    assert step1.offset == 4
    assert step1.action == "another test"
    assert step1.value == "test value"
    assert step1.target is None

    step2 = type4.steps[2]
    assert step2.offset == 6
    assert step2.action == "yet another test"
    assert step2.value == "yet another test value"
    assert step2.target == "test target"
