import pytest
import hftd

@pytest.mark.parametrize("input,expected_tick,expected_action,expected_string", [
    ("T00: connect", 0, "connect", "T00: connect"),
    ("T00 connect", 0, "connect", "T00: connect"),
    ("0 connect", 0, "connect", "T00: connect"),
    ("0: connect", 0, "connect", "T00: connect"),
    ("10 disconnect", 10, "disconnect", "T10: disconnect"),
    ("  t2  Download from port 1", 2, "download from port 1", "T02: download from port 1"),
    ("27 DoSomeThingYouStUpIdThIng", 27, "dosomethingyoustupidthing", "T27: dosomethingyoustupidthing")
])
def test_valid_fromstring(input, expected_tick, expected_action, expected_string):
    command = hftd.servers.Command.fromString(input)
    assert command.tick == expected_tick
    assert command.action == expected_action
    assert str(command) == expected_string

@pytest.mark.parametrize("input", [
    "",
    "something",
    "s0 something",
    "t9999 something",
    "9t9 something",
    "99 t9 something",
])
def test_invalid_fromstring(input):
    with pytest.raises(hftd.servers.InvalidInputError):
        command = hftd.servers.Command.fromString(input)
