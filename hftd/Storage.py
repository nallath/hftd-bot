from .DefaultImports import *

from decimal import Decimal

import aiofiles

from .Signal import Signal

log = logging.getLogger(__name__)

DataPath = NewType("DataPath", Path)

def decimal_representer(dumper, data):
    return dumper.represent_scalar("!decimal", "{}".format(data))

def decimal_constructor(loader, node):
    value = loader.construct_scalar(node)
    return Decimal(value)

yaml.add_representer(Decimal, decimal_representer)
yaml.add_constructor("!decimal", decimal_constructor)

class Storage():
    data_path = Path(__file__).parent.joinpath("../data")

    def __init__(self, path: Path, *, create: bool = False) -> None:
        self.__path = path
        self.__dirty = False
        self.__data = {}
        self.__create = create
        self.__loaded = False

    @property
    def path(self) -> Path:
        return self.__path

    @property
    def data(self) -> Dict[Any, Any]:
        return self.__data

    @data.setter
    def data(self, new_data: Dict[Any, Any]) -> None:
        self.__data = new_data
        self.__dirty = True

    @property
    def dirty(self) -> bool:
        return self.__dirty

    def mark_dirty(self) -> None:
        self.__dirty = True

    async def load(self) -> None:
        if not self.__path.exists() and self.__create:
            self.__path.touch()

        async with aiofiles.open(self.__path, "r") as f:
            file_data = await f.read()
            self.__data = yaml.full_load(file_data)
        self.__dirty = False

    def load_sync(self) -> None:
        if not self.__path.exists() and self.__create:
            self.__path.touch()

        with open(self.__path, "r") as f:
            self.__data = yaml.full_load(f)
        self.__dirty = False

    async def save(self) -> None:
        if not self.__dirty:
            return

        file_data = yaml.dump(self.__data)

        temp_path = self.__path.with_name(self.__path.name + ".tmp")
        async with aiofiles.open(temp_path, "w") as temp_file:
            await temp_file.write(file_data)

        temp_path.rename(self.__path)

        self.__dirty = False

class ValueStorage(Storage):
    def __init__(self, path: Path, log_path: Path):
        super().__init__(path, create = True)
        self.__log_path = log_path
        self.__transaction_log = None

    async def set(self, entity: str, amount: Decimal) -> None:
        self.data[entity] = amount
        self.mark_dirty()

        time = datetime.datetime.utcnow()
        await self.__transaction_log.write(f"{time}: {entity} has been set to {amount}\n")

    async def modify(self, entity: str, amount: Decimal, *, log_message: Optional[str] = "", transaction_message: str = "") -> None:
        if entity not in self.data:
            self.data[entity] = Decimal("0.00")

        self.data[entity] += amount
        self.mark_dirty()

        time = datetime.datetime.utcnow()
        message = log_message
        if message is not None:
            if not message:
                message = f"{entity} was modified by {amount}, new value is {self.data[entity]}"

            await self.__transaction_log.write(f"{time}: {message} {transaction_message}\n")

        await self.modified.emit(entity, amount, transaction_message)

    modified = Signal()

    def has_value(self, entity: str) -> bool:
        return entity in self.data

    def value(self, entity: str) -> Optional[Decimal]:
        if entity in self.data:
            return self.data[entity]
        return None

    def entities(self) -> List[str]:
        return list(self.data.keys())

    async def load(self) -> None:
        await super().load()
        if not isinstance(self.data, dict):
            self.data = {}
        self.__transaction_log = await aiofiles.open(self.__log_path, "a")

    async def save(self) -> None:
        await super().save()
        await self.__transaction_log.flush()
