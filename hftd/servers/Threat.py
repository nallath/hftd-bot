from ..DefaultImports import *

from ..Message import Message

from .ThreatTypes import ThreatType, ThreatAction

class Threat:
    def __init__(self, server: "Server", threat_type: ThreatType, data):
        self.__server = server

        self.__type = threat_type

        parts = data["time"].split(":")
        self.__report_time = datetime.timedelta(minutes = int(parts[0]), seconds = int(parts[1]))

        self.__security_system = data.get("security_system", "")
        self.__port = data.get("port", 0)
        self.__target = data.get("target", 0)
        self.__start = data.get("start", 0)
        self.__health = data.get("health", self.__type.health)
        self.__damage_resist = data.get("damage_resist", self.__type.damage_resist)

        self.__reported = False

        self.__steps = []
        for step in self.__type.steps:
            tick = self.__start + step.offset - 1
            self.__steps.append((tick, step))

        self.__make_report()

    @property
    def name(self) -> str:
        return self.__type.name

    @property
    def report_time(self) -> datetime.timedelta:
        return self.__report_time

    @property
    def type(self) -> ThreatType:
        return self.__type

    @property
    def server(self) -> "Server":
        return self.__server

    @property
    def start(self) -> int:
        return self.__start

    @property
    def health(self) -> int:
        return self.__health

    @property
    def damage_resist(self) -> int:
        return self.__damage_resist

    @property
    def steps(self) -> Dict[int, ThreatAction]:
        return self.__steps

    @property
    def reported(self) -> bool:
        return self.__reported

    @reported.setter
    def reported(self, reported: bool) -> None:
        self.__reported = reported

    @property
    def report(self) -> Message:
        return self.__report

    @property
    def security_system(self) -> int:
        return self.__security_system

    @property
    def target(self) -> int:
        return self.__target

    @property
    def port(self) -> int:
        return self.__port

    def __make_report(self) -> Message:
        message = self.__report_template.replace("<initial>", self.type.initial)

        steps = []
        for tick, step in self.__steps:
            steps.append(step.report(tick, self))

        properties = {}
        for property_name in dir(self):
            if property_name.startswith("__") or property_name in self.__skip_properties:
                continue
            properties[property_name] = getattr(self, property_name)

        self.__report = Message(message).processed(
            **properties,
            steps = steps,
        )

    __report_template = """```markdown
### Threat Detected: {{ name }} ###
<initial>

{%- for step in steps %}
- {{ step }}
{%- endfor %}
```"""
    __skip_properties = ("steps", "report")
