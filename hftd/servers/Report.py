from typing import Dict, List

import discord
from discord.ext import commands
import jinja2

from ..Storage import Storage

from .Command import Command

class Report:
    def __init__(self, server: "ActiveServer") -> None:
        self.__server = server
        self.__template = Storage(Storage.data_path.joinpath("report.yml"))

    async def generate(self, context: commands.Context) -> None:
        await self.__template.load()

        threat_active_ticks = {}
        for threat in self.__server.threats:
            if threat.start not in threat_active_ticks:
                threat_active_ticks[threat.start] = []
            threat_active_ticks[threat.start].append(threat)

        member_ticks = {}
        plans = self.__server.plans
        priorities = self.__server.priority

        for member in priorities:
            plan = plans[member]
            for command in plan:
                if command.tick not in member_ticks:
                    member_ticks[command.tick] = []
                member_ticks[command.tick].append((member, command))

        threat_action_ticks = {}
        for threat in self.__server.threats:
            for tick, step in threat.steps:
                if tick not in threat_action_ticks:
                    threat_action_ticks[tick] = []
                threat_action_ticks[tick].append((threat, step))

        plan_length = max(max(threat_action_ticks), max(member_ticks), max(threat_action_ticks)) + 1

        header_template = jinja2.Template(self.__template.data["header"])
        await context.send(header_template.render(server = self.__server, plan_length = plan_length))

        tick_begin_template = jinja2.Template(self.__template.data["tick_begin"])
        tick_end_template = jinja2.Template(self.__template.data["tick_end"])

        for tick in range(plan_length):
            await context.send(tick_begin_template.render(server = self.__server, tick = tick))

            if tick in threat_active_ticks:
                for threat in threat_active_ticks[tick]:
                    await threat.report.send(context.channel)
            else:
                await context.send("```No new active threats.```")

            message = ""
            if tick in member_ticks:
                for member, command in member_ticks[tick]:
                    message += f"{member.display_name}: {command.action}\n"

            if message:
                await context.send(f"```\n{message}\n```\n")
            else:
                await context.send("```No member performed any actions.```")

            message = ""
            if tick in threat_action_ticks:
                for threat, step in threat_action_ticks[tick]:
                    message += f"{threat.name}: {step.report(tick, threat)}\n"

            if message:
                await context.send(f"```\n{message}\n```\n")
            else:
                await context.send("```No threats active.```")

            await context.send(tick_end_template.render(server = self.__server, tick = tick))

        footer_template = jinja2.Template(self.__template.data["footer"])
        await context.send(footer_template.render(server = self.__server))
