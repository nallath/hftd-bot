import discord
import re

class InvalidInputError(RuntimeError):
    pass

class Command():
    def __init__(self, tick: int, action: str) -> None:
        self.__tick:int = tick
        self.__action:str = action

    @property
    def tick(self) -> int:
        return self.__tick

    @property
    def action(self) -> str:
        return self.__action

    def __str__(self) -> str:
        return f"T{self.__tick:0>2}: {self.__action}"

    @classmethod
    def fromString(cls, input: str) -> "Command":
        input = input.lower().strip()

        match = cls.tick_format.match(input)
        if not match:
            raise InvalidInputError(f"Did not understand `{input}`")

        try:
            tick = int(match.group(1))
        except ValueError as e:
            raise InvalidInputError(f"Did not understand `{input}`") from e

        if tick < 0 or tick > 99:
            raise InvalidInputError(f"Tick {tick} out of range in `{input}`")

        if cls.tick_format.match(input[match.end():]):
            raise InvalidInputError(f"Multiple ticks specified in `{input}`")

        return Command(tick, input[match.end():].strip())

    tick_format = re.compile(R"[tT]?([0-9]+)[:;.,]? ")
