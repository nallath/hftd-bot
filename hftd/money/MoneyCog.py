from ..DefaultImports import *

import aiofiles

from .. import Checks
from ..Storage import Storage

log = logging.getLogger(__name__)

class MoneyCog(commands.Cog, name = "Money"):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

        self.__data = {}
        self.__storage = Storage(Storage.data_path.joinpath("money.yml"), create = True)

        self.__transaction_log = None

    @commands.group()
    async def money(self, context: commands.Context) -> None:
        pass

    @money.command()
    @Checks.check_admin()
    async def add(self, context: commands.Context, member: discord.Member, amount: float) -> None:
        if member not in self.__data:
            self.__data[member] = 0.0

        self.__data[member] += amount
        self.__storage.mark_dirty()

        time = datetime.datetime.utcnow()
        await self.__transaction_log.write(f"{time}: ₿{amount} was added to {member.display_name}\n")

        dm_channel = await member.create_dm()
        await dm_channel.send(f"You received ₿{amount} in your BitMint wallet.")

        await context.send(f"Added ₿{amount} to {member.display_name}'s wallet.")

    @money.command()
    @Checks.check_admin()
    async def remove(self, context: commands.Context, member: discord.Member, amount: float) -> None:
        if member not in self.__data:
            self.__data[member] = 0.0

        self.__data[member] -= amount
        self.__storage.mark_dirty()

        time = datetime.datetime.utcnow()
        await self.__transaction_log.write(f"{time}: ₿{amount} was removed from {member.display_name}\n")

        dm_channel = await member.create_dm()
        await dm_channel.send(f"₿{amount} was removed from your BitMint wallet.")

        await context.send(f"Removed ₿{amount} from {member.display_name}'s wallet.")

    @money.command()
    @Checks.check_admin()
    async def show(self, context: commands.Context, members: commands.Greedy[discord.Member]) -> None:
        if not members:
            members = self.__data.keys()

        message = "```\n"
        for member in members:
            if member in self.__data:
                message += f"{member.display_name}: ₿{self.__data[member]}\n"
            else:
                message += f"{member.display_name} has no money.\n"
        message += "```\n"

        await context.send(message)

    @money.command()
    @commands.dm_only()
    async def balance(self, context: commands.Context) -> None:
        if context.author not in self.__data:
            await context.send("You have no BitMint.")
            return

        await context.send(f"Your BitMint wallet contains ₿{self.__data[context.author]}.")

    @money.command()
    @commands.dm_only()
    async def transfer(self, context: commands.Context, to: discord.Member, amount: float) -> None:
        if context.author not in self.__data or self.__data[context.author] < amount:
            await context.send("You do not have enough BitMint to do that.")
            return

        if amount <= 0.0:
            await context.send("You cannot transfer negative or zero amounts of BitMint")
            return

        if to not in self.__data:
            self.__data[to] = 0.0

        self.__data[to] += amount
        self.__data[context.author] -= amount
        self.__storage.mark_dirty()

        time = datetime.datetime.utcnow()
        await self.__transaction_log.write(f"{time}: {context.author.display_name} transferred ₿{amount} to {to.display_name}\n")

        dm_channel = await to.create_dm()
        await dm_channel.send(f"You received ₿{amount} in your BitMint wallet.")

        await context.send(f"Transferred ₿{amount} to {to.display_name}")

    @transfer.error
    async def transfer_error(self, context: commands.Context, exception: commands.CommandError) -> None:
        if isinstance(exception, commands.BadArgument) and "Member" in str(exception):
            await context.send("User not found")
        else:
            raise exception

    async def load(self) -> None:
        await self.__storage.load()

        self.__transaction_log = await aiofiles.open(Storage.data_path.joinpath("transactions.log"), "a")

        if not self.__storage.data:
            return

        for name, amount in self.__storage.data.items():
            member = None
            for guild in self.bot.guilds:
                member = guild.get_member_named(name)
                if member:
                    break

            if not member:
                log.warning(f"Could not find member named {name}")
                continue

            self.__data[member] = amount

    async def save(self) -> None:
        if not self.__storage.dirty:
            return

        if not self.__storage.data:
            self.__storage.data = {}

        for member, amount in self.__data.items():
            self.__storage.data[str(member)] = amount

        await self.__storage.save()

        await self.__transaction_log.flush()
