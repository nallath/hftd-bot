from .DefaultImports import *

import dataclasses

@dataclass(frozen = True)
class Message:
    message_parts: List[str] = dataclasses.field(init = False)

    message: dataclasses.InitVar[Union[str, List[str]]]

    def __post_init__(self, message) -> None:
        if isinstance(message, str):
            object.__setattr__(self, "message_parts", [message])
        else:
            object.__setattr__(self, "message_parts", message)

    def processed(self, **template_arguments) -> "Message":
        message_parts = []
        for part in self.message_parts:
            message_parts.append(jinja2.Template(part).render(**template_arguments))
        return dataclasses.replace(self, message = message_parts)

    async def send(self, channel: discord.TextChannel):
        for message in self.message_parts:
            if message:
                await channel.send(message)
