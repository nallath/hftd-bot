from ..DefaultImports import *
import copy

from discord.ext import tasks

from ..Signal import Signal

from .ServerType import ServerType

class ServerState(IntEnum):
    Created = 1
    Started = 2
    Scanning = 3
    Planning = 4
    Ready = 5
    Running = 6
    Finished = 7
    Timeout = 8

TimeStep = 10

class Server:
    def __init__(self, name: str, type: ServerType):
        self.__name: str = name
        self.__type = type
        self.__state: int = ServerState.Created

        self.__members = []
        self.__plans = {}
        self.__running = {}

        self.__elapsed_time = datetime.timedelta(seconds = 0)
        self.__countdown = tasks.loop(seconds = TimeStep, count = None)(self.countdown)
        self.__countdown.after_loop(self.countdown_end)

        self.__ports = copy.deepcopy(type.ports)
        self.__threats = copy.deepcopy(type.threats)

    @property
    def name(self) -> str:
        return self.__name

    @property
    def type(self):
        return self.__type

    @property
    def state(self) -> ServerState:
        return self.__state

    stateChanged = Signal()

    message = Signal()

    async def connect(self):
        await self.message.emit(self.type.connect_message)
        self.__state = ServerState.Started
        await self.stateChanged.emit()

    async def scan(self):
        self.__countdown.start()
        await self.message.emit(self.type.scan_result_message)
        self.__state = ServerState.Scanning
        await self.stateChanged.emit()

    async def set_plan(self, member, plan):
        if self.__state < ServerState.Planning:
            self.__state = ServerState.Planning
            await self.stateChanged.emit()

        member_id = self.__members.index(member)

        self.__plans[member_id] = plan

        if len(self.__plans) == len(self.__members):
            self.message.emit(self.type.planning_done_message)
            self.__state = ServerState.Ready
            await self.stateChanged.emit()

    async def run_plan(self, member):
        if self.__state < ServerState.Running:
            self.__state = ServerState.Running
            await self.stateChanged.emit()

        member_id = self.__members.index(member)

        self.__running[member_id] = True

        if len(self.__running) == len(self.__members):
            self.message.emit(self.type.finished_success_message)
            self.__state = ServerState.Finished
            await self.stateChanged.emit()

    def add_member(self, member: str) -> None:
        self.__members.append(member)

    def remove_member(self, member: str) -> None:
        member_id = self.__members.index(member)

        del self.__members[member_id]
        if member_id in self.__plans:
            del self.__plans[member_id]
        if member_id in self.__running:
            del self.__running[member_id]

    async def countdown(self):
        self.__elapsed_time += datetime.timedelta(seconds = TimeStep)
        if self.__elapsed_time >= self.__type.duration:
            self.__countdown.stop()

        #for threat in self.__threats:
            #if threat.report_time <= self.__elapsed_time and not threat.reported:
                #await threat.report.send(self.__text_channel)
                #threat.reported = True

        remaining = (self.__server.duration - self.__elapsed_time).seconds
        if remaining <= 60:
            if remaining % 10 == 0:
                self.message.emit(f"{remaining} seconds remaining.")
        elif remaining % 60 == 0:
            self.message.emit(f"{remaining // 60} minutes remaining.")

    async def countdown_end(self):
        if self.__state == ServerState.Finished:
            return

        for i in range(len(self.__members)):
            if i not in self.__running:
                self.__running[i] = True

        self.__state = ServerState.Timeout
        await self.stateChanged.emit()

        #self.__server_finished = True
        #await self.__server.finished_timeout.send(self.__text_channel)

    #def member_id(self, member: str) -> int:
        #try:
            #index = self.__members.index(member)
            #return index
        #except ValueError:
            #return -1

#class AI:
    #def __init__(self):
        #self.__controller = ""
        #self.__plan = []
        #self.__running = False

    #@property
    #def controller(self) -> str:
        #return self.__controller

    #@controller.setter
    #def controller(self, controller: str) -> None:
        #self.__controller = controller

    #@property
    #def plan(self) -> List[str]:
        #return self.__plan

    #@plan.setter
    #def plan(self, plan: List[str]) -> None:
        #self.__plan = plan

    #@property
    #def running(self) -> bool:
        #return self.__running

    #@running.setter
    #def running(self, running: bool) -> None:
        #self.__running = running
