from typing import Optional
import asyncio
import pathlib
import urllib.parse
import logging

import discord
from discord.ext import commands
import jinja2

from ..Storage import Storage

from .Character import Character

log = logging.getLogger("characters")

class CharactersCog(commands.Cog, name = "Characters"):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

        self.__characters = {}
        self.__greeted = set()
        self.__messages = {}

        self.__characters_file = Storage(Storage.data_path.joinpath("characters", "characters.yml"))

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member) -> None:
        if (self.has_character(member)):
            return

        channel = await member.create_dm()
        await channel.send(self.__messages["welcome"])

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message) -> None:
        if message.channel.type != discord.ChannelType.private:
            return

        if message.author == self.bot.user:
            return

        if message.author not in self.__characters:
            return

        character = self.__characters[message.author]
        if not character.is_creating:
            return

        if not character.name:
            character.name = message.content
            await self.bot.send_templated_message(
                message.channel,
                self.__messages["character_collective"],
                collectives = ", ".join(self.__collectives)
            )
            return

        if not character.collective:
            if message.content not in self.__collectives:
                await self.bot.send_templated_message(
                    message.channel,
                    self.__messages["character_collective_error"],
                    collectives = ", ".join(self.__collectives)
                )
                return

            character.collective = message.content
            await message.channel.send(self.__messages["character_description"])
            return

        if not character.description:
            character.description = message.content
            await self.bot.send_templated_message(message.channel, self.__messages["character_ready"], character = character)
            return

        if message.content == "reset":
            character.name = ""
            character.collective = ""
            character.description = ""
            await message.channel.send(self.__messages["character_name"])
            return

        if message.content == "finish":
            await self.__finish_character(message, character)
            return

    @commands.command()
    @commands.dm_only()
    async def create_character(self, context) -> None:
        if (self.has_character(context.channel.recipient)):
            await self.bot.send_templated_message(
                context.channel,
                self.__messages["has_character"],
                character = self.__characters[context.channel.recipient]
            )
            return

        await context.send(self.__messages["begin_character_creation"])

        self.__characters[context.channel.recipient] = Character()
        self.__characters[context.channel.recipient].member = context.channel.recipient
        self.__characters[context.channel.recipient].is_creating = True

        log.info(f"Started character creation for {context.channel.recipient}")

        await context.send(self.__messages["character_name"])

    @commands.command()
    @commands.dm_only()
    async def character(self, context) -> None:
        if (not self.has_character(context.channel.recipient)):
            await context.send("You have no character.")
            return

        await self.bot.send_templated_message(
            context.channel,
            self.__messages["character_details"],
            character = self.__characters[context.channel.recipient]
        )

    @commands.command()
    @commands.has_permissions(kick_members = True)
    async def characters(self, context) -> None:
        for user, character in self.__characters.items():
            await self.bot.send_templated_message(
                context.channel,
                self.__messages["list_details"],
                user = user,
                character = character
            )

    @commands.command()
    @commands.has_permissions(kick_members = True)
    async def character_details(self, context, member: discord.Member) -> None:
        if member not in self.__characters:
            await context.send("That member has no character.")

        await self.bot.send_templated_message(
            context.channel,
            self.__messages["character_details"],
            character = self.__characters[member]
        )

    def has_character(self, member: discord.Member):
        if member in self.__characters:
            return True

        return False

    async def load(self):
        await self.__characters_file.load()
        self.__messages = self.__characters_file.data["messages"]
        self.__collectives = self.__characters_file.data["collectives"]
        self.__grants = self.__characters_file.data["grant_roles"]

        path = Storage.data_path.joinpath("characters")
        for file_path in path.iterdir():
            if file_path == self.__characters_file.path:
                continue

            character = Character(file_path)
            await character.load()
            character.member = self.bot.guilds[0].get_member_named(character.member)

            self.__characters[character.member] = character

    async def save(self):
        await self.__characters_file.save()

        for user, character in self.__characters.items():
            if not character.is_complete():
                continue

            await character.save()

    async def __finish_character(self, message, character):
        character.path = self.__characters_file.path.with_name(urllib.parse.quote_plus(character.name) + ".yml")
        character.dirty = True
        character.is_creating = False
        await self.bot.send_templated_message(message.channel, self.__messages["end_character_creation"], character = character)

        grant_roles = []
        for role in self.__grants:
            if role.startswith("{"):
                grant_roles.append(getattr(character, role.strip(" {}")))
            else:
                grant_roles.append(role)

        for guild in self.bot.guilds:
            def to_guild_role(role_name):
                for role in guild.roles:
                    if role.name == role_name:
                        return role
                return ""

            actual_roles = list(map(to_guild_role, grant_roles))

            member = guild.get_member(message.author.id)
            await member.add_roles(*actual_roles)

        log.info(f"Completed creation of character {character.name} for {message.author}")
