from ..DefaultImports import *

from decimal import Decimal

import aiofiles

from ..Bot import Bot
from ..Serializable import Serializable
from ..Storage import Storage, ValueStorage, DataPath
from .. import Checks

log = logging.getLogger(__name__)

MoneyConfiguration = NewType("MoneyConfiguration", Storage)
MoneyData = NewType("MoneyData", ValueStorage)

Wallet = Union[discord.Member, str]

class MoneyModule(injector.Module):
    @injector.singleton
    @injector.provider
    def configuration(self, data_path: DataPath) -> MoneyConfiguration:
        return Storage(data_path.joinpath("money.yml"))

    @injector.singleton
    @injector.provider
    def wallets(self, data_path: DataPath) -> MoneyData:
        return ValueStorage(data_path.joinpath("wallets.yml"), data_path.joinpath("transactions.log"))

@injector.singleton
class Money(Serializable, commands.Cog):
    @injector.inject
    def __init__(self, bot: Bot, configuration: MoneyConfiguration, wallets: MoneyData) -> None:
        self.bot = bot

        self.__data = {}
        self.__config = configuration
        self.__wallets = wallets
        self.__wallets.modified.connect(self.wallet_modified)
        self.__known_wallets = []

        self.__currency_symbol = "₿"

    @commands.group(case_insensitive = True)
    async def money(self, context: commands.Context) -> None:
        """
        Commands related to money.
        """
        pass

    @money.command()
    @Checks.check_admin()
    async def add(self, context: commands.Context, wallet: Wallet, amount: Decimal, *, message: str = "") -> None:
        """
        Add money to a wallet.

        This command adds money to a wallet. A wallet can either be a member, a
        troupe or a collective.

        :param wallet: The wallet to add money to.
        :param amount: The amount of money to add.
        :param message: An optional message that is displayed to the owner of the wallet.
        """

        if isinstance(wallet, discord.Member):
            wallet = wallet.display_name
        elif wallet not in self.__known_wallets:
            await context.send("No such wallet.")
            return

        log_message = f"{self.format_currency(amount)} was added to {wallet}"
        await self.__wallets.modify(wallet, amount, log_message = log_message, transaction_message = message)
        await context.send(log_message)

    @money.command()
    @Checks.check_admin()
    async def remove(self, context: commands.Context, wallet: Wallet, amount: Decimal, *, message: str = "") -> None:
        """
        Remove money from a wallet.

        This command removes money from a wallet. A wallet can either be a member, a
        troupe or a collective.

        :param wallet: The wallet to remove money from.
        :param amount: The amount of money to remove.
        :param message: An optional message that is displayed to the owner of the wallet.
        """

        if isinstance(wallet, discord.Member):
            wallet = wallet.display_name
        elif wallet not in self.__known_wallets:
            await context.send("No such wallet.")
            return

        log_message = f"{self.format_currency(amount)} was removed from {wallet}"
        await self.__wallets.modify(wallet, -amount, log_message = log_message, transaction_message = message)
        await context.send(log_message)

    @money.command()
    async def show(self, context: commands.Context, wallets: commands.Greedy[Wallet]) -> None:
        """
        Show the amount of money in a list of wallets.

        This command will display the amount of money that is in each specified wallet.

        :param wallets: The wallets to show the contents of.

        :note: This command can only be used to show the contents of troupe and collective
               wallets you have access to.
        """

        is_admin = self.bot.is_admin(context)
        member = self.bot.find_member(context.author)

        if not wallets:
            if is_admin:
                wallets = self.__wallets.entities()
            else:
                await context.send("Please specify which wallet to show.")
                return

        allowed = is_admin
        for wallet in wallets:
            if isinstance(wallet, discord.Member):
                break

            role = discord.utils.find(lambda r: r.name == wallet, member.roles)
            if not role:
                break
        else:
            allowed = True

        if not allowed:
            await context.send("You are only allowed to show wallets of your troupe or your collective.")
            return

        message = "```\n"
        for w in wallets:
            wallet = w.display_name if isinstance(w, discord.Member) else w
            if self.__wallets.has_value(wallet):
                message += f"{wallet}: {self.format_currency(self.__wallets.value(wallet))}\n"
            else:
                message += f"{wallet} has no money.\n"
        message += "```\n"

        await context.send(message)

    @money.command()
    @commands.dm_only()
    async def balance(self, context: commands.Context) -> None:
        """
        Show the balance of your wallet.

        :note: This command can only be used in direct messages.
        """
        member = self.bot.find_member(str(context.author))
        if not self.__wallets.has_value(member.display_name):
            await context.send("You have no BitMint.")
            return

        await context.send(f"Your BitMint wallet contains {self.format_currency(self.__wallets.value(member.display_name))}.")

    @money.command()
    @commands.dm_only()
    async def transfer(self, context: commands.Context, destination: Wallet, amount: Decimal, *, message: str = "") -> None:
        """
        Transfer an amount from your wallet to another wallet.

        This command will transfer a specified amount of money from your wallet to
        another wallet, optionally sending them a message along with the amount.
        A wallet can either be another member, a troupe or a collective.

        :param destination: The destination wallet.
        :param amount: The amount to send.
        :param message: The message to display along with the transfer.

        :note: This command can only be used in direct messages.
        """
        member = self.bot.find_member(str(context.author))
        if not member:
            return

        member = member.display_name

        if isinstance(destination, discord.Member):
            destination = destination.display_name
        elif destination not in self.__known_wallets:
            await context.send("No such wallet.")
            return

        if not self.__wallets.has_value(member) or self.__wallets.value(member) < amount:
            await context.send("You do not have enough BitMint to do that.")
            return

        if amount <= Decimal(0):
            await context.send("You cannot transfer negative or zero amounts of BitMint")
            return

        log_message = f"{member} transferred {self.format_currency(amount)} to {destination}."
        await self.__wallets.modify(destination, amount, log_message = log_message, transaction_message = message)
        with self.__wallets.modified.block():
            await self.__wallets.modify(member, -amount, log_message = None)

        await context.send(f"Transferred {self.format_currency(amount)} to {destination}.")
        await context.send(f"Your new balance is {self.format_currency(self.__wallets.value(member))}")

    @transfer.error
    async def transfer_error(self, context: commands.Context, exception: commands.CommandError) -> None:
        if isinstance(exception, commands.BadArgument) and "Member" in str(exception):
            await context.send("Destination not found")
        else:
            await context.send(str(exception))

    async def load(self) -> None:
        await self.__config.load()
        await self.__wallets.load()

        self.__known_wallets = self.__config.data.get("wallets", [])

        for wallet in self.__known_wallets:
            if not self.__wallets.has_value(wallet):
                await self.__wallets.set(wallet, Decimal("0.00"))

    async def save(self) -> None:
        await self.__wallets.save()

    async def wallet_modified(self, entity: str, amount: Decimal, message: str) -> None:
        member = self.bot.find_member(entity)
        if not member:
            return

        dm = ""
        if amount > 0:
            dm = f"You received {self.format_currency(amount)} in your BitMint wallet.\n"
        else:
            dm = f"{self.format_currency(amount)} was removed from your BitMint wallet.\n"

        if message:
            dm += message + "\n"

        dm += f"Your new balance is {self.format_currency(self.__wallets.value(entity))}"

        await member.send(dm)

    def format_currency(self, amount: Decimal) -> str:
        digit_count = len(amount.as_tuple().digits)
        if digit_count <= 3:
            return f"{self.__currency_symbol}{amount:.2f}"
        else:
            return f"{self.__currency_symbol}{amount}"
