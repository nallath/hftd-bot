from ..DefaultImports import *

import subprocess

from ..Bot import Bot
from ..Storage import DataPath
from .. import Checks

@injector.singleton
class BotCog(commands.Cog, name = "Bot"):
    @injector.inject
    def __init__(self, bot: Bot, data_path: DataPath) -> None:
        self.bot = bot
        self.__data_path = data_path

    @commands.group(case_insensitive = True)
    @Checks.check_admin()
    async def bot(self, context: commands.Context) -> None:
        """
        Generic bot helper commands.
        """
        pass

    @bot.command()
    async def status(self, context: commands.Context) -> None:
        """
        Display the bot's status.
        """
        await context.send("Bot running")
        #if "Servers" in self.bot.cogs:
            #active = self.bot.get_cog("Servers").active_server_count
            #await context.send(f"Servers active: {active}")

    @bot.command()
    async def say(self, context, channel: discord.TextChannel, *, message: str) -> None:
        """
        Say something in a certain channel.

        This will display the message in the specified channel, using the Bot's account.

        :param channel: The channel to display the message in.
        :param message: The message to display. Note that the maximum character limit for messages is 2000.
        """
        await channel.send(message)

    @bot.command()
    async def restart(self, context) -> None:
        """
        Restart the bot.

        This will shut down and restart the bot.
        """

        # Stop and close the event loop, relying on systemd to restart the service.
        await self.bot.close()

        self.bot.restarting = True

    @bot.command(name = "pull-data", aliases = ["pull_data"])
    async def pull_data(self, context) -> None:
        """
        Perform "git pull" on the data directory.

        This is just because I'm lazy and don't want to set up the deployment pipeline
        on Gitlab.
        """

        result = subprocess.run(["git", "pull"], capture_output = True, cwd = self.__data_path)
        message = f"```\n{result.stdout.decode()}\n{result.stderr.decode()}\n```"
        await context.send(message)
